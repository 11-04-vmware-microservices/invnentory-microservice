package com.classpath.inventorymicroservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
public class InventoryRestController {

    private int counter  = 1000;

    @PostMapping
    public int updateQty(){
        return --counter;
    }
    @GetMapping
    public int fetchCounter(){
        return counter;
    }
}
