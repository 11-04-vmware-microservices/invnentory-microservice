package com.classpath.inventorymicroservice.event;


import com.classpath.inventorymicroservice.model.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Builder
@ToString
public class OrderEvent {
    private Order order;
    private OrderStatus orderStatus;
}
