package com.classpath.inventorymicroservice.model;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Order {
    private Long id;
    private String name;
    private String email;
    private double price;
}
